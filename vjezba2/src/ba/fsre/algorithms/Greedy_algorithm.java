/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ba.fsre.algorithms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Danijel
 */
public class Greedy_algorithm {
    private int D = 30;
    public static void main(String[] args) throws FileNotFoundException
    {	
        Greedy_algorithm tmp = new Greedy_algorithm();
        tmp.Start();
    }
    
    public void Start(){
        try {
            createFile();
            racunaj();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Greedy_algorithm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void racunaj() throws FileNotFoundException
    {
        PrintWriter out = new PrintWriter(new File("zaustavljanja.txt"));
        File udaljenosti = new File("udaljenosti.txt");
        Scanner scanner1 = new Scanner(udaljenosti);
        int i = 0,j = 2,udaljenost;
        ArrayList <Integer> duljina = new ArrayList<Integer>();
        udaljenost = scanner1.nextInt();
        out.print("Put 1: ");
        while(i<10000)
        {
            if(!scanner1.hasNext("0"))
            {
                while(udaljenost<D && !scanner1.hasNext("0"))
                {
                    duljina.clear();
                    duljina.add(scanner1.nextInt());
                    udaljenost = udaljenost + duljina.get(0);
                    j++;
                }
                if(!scanner1.hasNext("0"))
                {
                    udaljenost = duljina.get(0);
                    duljina.clear();
                    out.print(j-1);
                    out.print(" ");
                }
                else
                {
                    if(udaljenost >= D)
                    {
                        out.print(j-1);
                        out.print(" ");
                    }
                    out.println();
                    udaljenost=scanner1.nextInt();
                    j = 1;
                    i = i+1;
                    if(i!=10000)
                    out.print("Put "+ (i+1) + ": ");
                }
            }
            else
            {
                out.println();
                udaljenost = scanner1.nextInt();
                j = 2;

            }
        }
        out.close();
    }
    
    private void createFile() throws FileNotFoundException
    {
        PrintWriter out = new PrintWriter (new File("udaljenosti.txt"));
        int i = 0;
        int a,j;
        
        int max = 10; 
        int min = 5; 
        int range = max - min + 1;
        
        while(i<10000)
        {
            j=0;
            a = (int)(Math.random()*range)+min;
            while(j<a)
            {
                int b = (int)(Math.random()*(D-1)+1);
                out.print(b);
                out.print(" ");
                j=j+1;
            }
            out.print("0");
            out.println();
            i++;
        }
        out.close();
        System.out.println("Datoteka udaljenosti je kreirana.");
    }
}
