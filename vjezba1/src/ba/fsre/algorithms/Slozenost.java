/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ba.fsre.algorithms;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;
/**
 *
 * @author Danijel
 */
public class Slozenost {
    private int N;
    private int[] _niz;
    
    public Slozenost(int n){
        this.N = n;
        this._niz = new int[this.N];
        Random rnd = new Random();
        
        for(int i=0; i<N; i++)
            this._niz[i] = rnd.nextInt(1000);
        
        Arrays.sort(this._niz);
    }

    public void Funkcija(int n){
        long suma=1;
        long X = 1;
        long br = 0;
        for(long i=0;i<n; i++){
            suma +=1;
            X *= 2;
            br++;
        }
        System.out.println("funkcija");
        System.out.println(String.format("n = %d, T(n) = %d", n, br));
    }


    public void Funkcija2(int n){
        long suma=1;
        long X = 1;
        long br=0;
        for(long i=0;i<n; i++){
          for(long j=0;j<n;j++){
            suma +=1;
            X *= 2;
            br++;
          }
        }
        System.out.println("funkcija2");
        System.out.println(String.format("n = %d, T(n) = %d", n, br));
    }


    public void Funkcija3(int n){
        long suma=1;
        long X = 1;
        long br=1;
        for(long i=0;i<n; i++){
          for(long j=0;j<X;j++) {
            suma +=1;
            br++;
          }
         X *= 2;
        }
        System.out.println("funkcija3");
        System.out.println(String.format("n = %d, T(n) = %d", n, br));
    }


    private void Znamenke(){
        int pom;
        int br=0;
        for(int i=0; i< this._niz.length; i++){
            pom = this._niz[i];
            while(pom > 0){
                pom = pom/10;
                br++;
            }
        }
        System.out.println("znamenke");
        System.out.println(String.format("n = %d, T(n) = %d", this._niz.length, br));
    }

    public void Trazi(){
        int br=0;
        Random rnd = new Random();
        for(int i=0;i<this._niz.length; i++){
            int trazeni = rnd.nextInt(1000);
            int poc=0, kraj = this._niz.length-1, sredina;
            while(true){     
                sredina = (poc + kraj)/2;
                if(this._niz[sredina] == trazeni){
                    break;
                }
                if(poc >= kraj){
                    break;
                }	
                if(trazeni<this._niz[sredina]){
                        kraj = sredina-1;
                }else if(trazeni >= this._niz[sredina]){
                        poc = sredina +1;
                }
                br++;
            }
        }
        System.out.println("trazi");
        System.out.println(String.format("n = %d, T(n) = %d", this._niz.length, br));
    }

    public static void main(String args[]){

        int n = 10;
        Slozenost pom = new Slozenost(n);
        long startTime = System.currentTimeMillis();
        //pom.Funkcija(n);
        //pom.Funkcija2(n);
        //pom.Funkcija3(n);
        //pom.Znamenke();
        pom.Trazi();

        long time = System.currentTimeMillis() - startTime;
        System.out.println(String.format("Total time exectution: %d ms", time));
    }
}