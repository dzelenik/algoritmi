/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ba.fsre.algorithms;
/**
 *
 * @author Danijel
 */
public class Rekurzija {

    private void _znamenke(int n){
        System.out.println("n: " + n);
        if(n>0){
            int zn = n%10;
            System.out.println("Znamenka " + zn);
            _znamenke(n/10);
        }
        return;
    }
    
    private int _fakt(int n){
        int rez=0;
        if(n<=1){
            rez = 1;
        }else{
            rez = n * _fakt(n-1);
        }
        System.out.print(String.format("fakt %d", n));
        System.out.println(String.format("\treturn %d", rez));
        return rez;
    }
    
    private int _fib(int n){
        System.out.println(n);
        int rez = 0;
        if(n<=0)
            rez= 0;
        else if(n==1)
             rez= 1;
        else
             rez= _fib(n-1) + _fib(n-2);
        return rez;
    }
    
    public void Znamenke(int n){
        _znamenke(n);
    }
    
    public void Fakt(int n){
        int rez = _fakt(n);
        System.out.println(String.format("%d! = %d",n,rez));
    }
    
    public void Fib(int n){
        int rez = _fib(n);
        System.out.println(String.format("Fib %d = %d",n,rez));
    }
    
    public static void main(String args[]){
        Rekurzija pom = new Rekurzija();
        //pom.Znamenke(345);
        //pom.Fib(5);
        pom.Fakt(5);
    }

}
