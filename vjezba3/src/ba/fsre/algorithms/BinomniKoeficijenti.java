/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ba.fsre.algorithms;

/**
 *
 * @author Danijel
 */
public class BinomniKoeficijenti {
    
    private static int C(int n, int k)
    {
        if (k==0 || k==n)
            return 1;
        return C(n-1, k-1) + C(n-1, k);
    }
    
    private static int bcd(int n, int k){
        int[][] C = new int[n+1][n+1];
        int i, j;
        for (i = 0; i <= n; i++) 
            C[i][0] = 1;
        for (i = 0; i <= n; i++) 
            C[i][i] = 1;
        for (i = 1; i <= n; i++)
            for (j = 1; j <= i; j++)
                C[i][j] = C[i-1][j-1] + C[i-1][j];
        return C[n][k];
    }
    public static void main(String[] args)
    {
        int rez = bcd(5,2);
        System.out.println(rez);
    }
    
}
