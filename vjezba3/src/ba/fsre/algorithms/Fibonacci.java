/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ba.fsre.algorithms;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Danijel
 */
public class Fibonacci {
    
    private static int MAX = 100;
    private static int[] lookup = new int[MAX];
    
    private static int fib(int n){
        if(n<=1)
            return n;
        else 
            return fib(n-1) + fib(n-1);
    }
    
    private static int fibd(int n){
        if(lookup[n] != -1)
            return lookup[n];
        if(n<=1)
            lookup[n]=n;
        else
            lookup[n] =  fibd(n-1)+ fibd(n-2);
        return lookup[n];
    }
    
    private static void init(int n){
        for(int i=0; i<=n; i++)
            lookup[i]= -1;
    }

    public static void main(String[] args)
    {	
        int n=6;
        init(n);
        int a = fibd(n);
        System.out.println(a);
    }
    
}
